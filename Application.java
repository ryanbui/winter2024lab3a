import java.util.Scanner;
public class Application {

	public static void main (String [] args){
		Student student1 = new Student();
		student1.name = "Ryan";
		student1.rScore = 34.9;
		student1.program = "Computer Science Technology";
		
		Student student2 = new Student();
		student2.name = "Jaden";
		student2.rScore = 27.6;
		student2.program = "Pure and Applied Science";
		System.out.println(student1.name);
		System.out.println(student1.rScore);
		System.out.println(student1.program);
		System.out.println(student2.name);
		System.out.println(student2.rScore);
		System.out.println(student2.program);
		
		Student [] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		System.out.println(section3[0].name);
		
		section3[2] = new Student();
		section3[2].name = "Nate";
		section3[2].program = "ALC";
		section3[2].rScore = 32.0;
		
		System.out.println(section3[2].name);
		System.out.println(section3[2].program);
		System.out.println(section3[2].rScore);
		
		student1.greetingToCS();
		student1.rateStudentRScore();
		student2.greetingToCS();
		student2.rateStudentRScore();
		
		
	}
}